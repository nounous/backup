#!/usr/bin/python3
import json
import os

import jinja2
import ldap


path = os.path.dirname(os.path.abspath(__file__))


def get_users(config, base):
    users_query_id = base.search(config['rootdn'], ldap.SCOPE_ONELEVEL, 'objectClass=posixAccount')
    users = base.result(users_query_id)[1]
    return [ user['uid'][0].decode('utf-8') for _, user in users ]

def get_timestamp(user,index,pool_size):
    delta = (24*3600 - 600) / pool_size
    h = int(index * delta + 600)
    hour, minute, second = h//3600%24, h//60%60, h%60
    return user,hour,minute,second

def ldap_init(config):
    base = ldap.initialize(config['ldap_server'])
    base.simple_bind_s(config['binddn'], config['password'])
    return base

def create_borg_configuration(config,user):
    config_path = os.path.join(path,f'generated/config.{ user }.yaml')
    with open(os.path.join(path,'templates','config.borg.yaml.j2')) as file:
        template = jinja2.Template(file.read())
    with open(config_path, 'w') as file:
        file.write(template.render(username=user, borg_key=config['borg_key']))

if __name__ == '__main__':
    with open(os.path.join(path,'backup.json')) as file:
        config = json.load(file)
    base = ldap_init(config)
    users = get_users(config, base)
    sorted(users)
    timestamps = [ get_timestamp(user,index,len(users)) for index,user in enumerate(users) ]
    with open(os.path.join(path,'templates','borg-home.j2')) as file:
        template = jinja2.Template(file.read())
    with open('/etc/cron.d/borg-home','w') as file:
        file.write(template.render(timestamps=timestamps, path=path))
    for user in users:
        create_borg_configuration(config,user)
